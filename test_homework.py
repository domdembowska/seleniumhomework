import string
import random

from selenium.webdriver import Chrome
from selenium.webdriver.chrome.service import Service
from selenium.webdriver.common.by import By
from webdriver_manager.chrome import ChromeDriverManager


# zalogowanie do Test Arena

def test_arena_login():
    service = Service(ChromeDriverManager().install())
    browser = Chrome(service=service)
    browser.fullscreen_window()
    browser.get('http://demo.testarena.pl/zaloguj')

    emailInput = browser.find_element(By.CSS_SELECTOR, "#email")
    emailInput.send_keys("administrator@testarena.pl")
    passwordInput = browser.find_element(By.CSS_SELECTOR, "#password")
    passwordInput.send_keys("sumXQQ72$L")
    submitButton = browser.find_element(By.CSS_SELECTOR, "#login")
    submitButton.click()
    browser.find_element(By.CSS_SELECTOR, ".header_admin").click()
# dodawanie nowego projektu
    browser.find_element(By.CSS_SELECTOR, ".button_link").click()

    def get_random_string(length):
        return ''.join(random.choices(string.ascii_uppercase + string.digits, k=length))

    project_name = get_random_string(10)
    prefix = get_random_string(5)
    description = get_random_string(20)
    browser.find_element(By.CSS_SELECTOR, "#name").send_keys(project_name)
    browser.find_element(By.CSS_SELECTOR, "#prefix").send_keys(prefix)
    browser.find_element(By.CSS_SELECTOR, "#description").send_keys(description)

    browser.find_element(By.CSS_SELECTOR, "#save").click()
    browser.find_element(By.CSS_SELECTOR, ".header_admin").click()

    browser.find_element(By.CSS_SELECTOR, ".activeMenu").click()


def verify_project_found(self, project_name, sleep=20):
    found_project = self.browser.find_elements(By.CSS_SELECTOR, "tbody tr")
    assert len(found_project) > 0

    names = self.browser.find_elements(By.CSS_SELECTOR, "tbody tr td:nth-of-type(1)")
    for name in names:
        assert project_name in name.text.lower()

    sleep.time(20)
    browser.quit()
